package torrent

import (
	"log"
	"strings"

	"github.com/anacrolix/missinggo/bitmap"
	"github.com/anacrolix/missinggo/pubsub"

	"github.com/anacrolix/torrent/metainfo"
	"github.com/anacrolix/torrent/mse"
	pp "github.com/anacrolix/torrent/peer_protocol"
)

// The torrent's infohash. This is fixed and cannot change. It uniquely
// identifies a torrent.
func (t *Torrent) InfoHash() metainfo.Hash {
	return t.infoHash
}

// Returns a channel that is closed when the info (.Info()) for the torrent
// has become available.
func (t *Torrent) GotInfo() <-chan struct{} {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return t.gotMetainfo.C()
}

// Returns the metainfo info dictionary, or nil if it's not yet available.
func (t *Torrent) Info() *metainfo.Info {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return t.info
}

func (t *Torrent) InfoBytes() []byte {
	return t.metadataBytes // return bencode.Marshal(t.info)
}

// Returns a Reader bound to the torrent's data. All read calls block until
// the data requested is actually available.
func (t *Torrent) NewReader() Reader {
	r := reader{
		mu:        &t.cl.mu,
		t:         t,
		readahead: 5 * 1024 * 1024,
		length:    *t.length,
	}
	t.addReader(&r)
	return &r
}

// Returns the state of pieces of the torrent. They are grouped into runs of
// same state. The sum of the state run lengths is the number of pieces
// in the torrent.
func (t *Torrent) PieceStateRuns() []PieceStateRun {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return t.pieceStateRuns()
}

func (t *Torrent) PieceState(piece int) PieceState {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return t.pieceState(piece)
}

// The number of pieces in the torrent. This requires that the info has been
// obtained first.
func (t *Torrent) NumPieces() int {
	return t.numPieces()
}

// Get missing bytes count for specific piece.
func (t *Torrent) PieceBytesMissing(piece int) int64 {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()

	return int64(t.pieces[piece].bytesLeft())
}

// Drop the torrent from the client, and close it. It's always safe to do
// this. No data corruption can, or should occur to either the torrent's data,
// or connected peers.
func (t *Torrent) Drop() {
	t.cl.mu.Lock()
	t.cl.dropTorrent(t.infoHash)
	t.cl.mu.Unlock()
}

// Number of bytes of the entire torrent we have completed. This is the sum of
// completed pieces, and dirtied chunks of incomplete pieces. Do not use this
// for download rate, as it can go down when pieces are lost or fail checks.
// Sample Torrent.Stats.DataBytesRead for actual file data download rate.
func (t *Torrent) BytesCompleted() int64 {
	t.cl.mu.RLock()
	defer t.cl.mu.RUnlock()
	return t.bytesCompleted()
}

// The subscription emits as (int) the index of pieces as their state changes.
// A state change is when the PieceState for a piece alters in value.
func (t *Torrent) SubscribePieceStateChanges() *pubsub.Subscription {
	return t.pieceStateChanges.Subscribe()
}

// Returns true if the torrent is currently being seeded. This occurs when the
// client is willing to upload without wanting anything in return.
func (t *Torrent) Seeding() bool {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return t.seeding()
}

// Clobbers the torrent display name. The display name is used as the torrent
// name if the metainfo is not available.
func (t *Torrent) SetDisplayName(dn string) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	t.setDisplayName(dn)
}

// The current working name for the torrent. Either the name in the info dict,
// or a display name given such as by the dn value in a magnet link, or "".
func (t *Torrent) Name() string {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return t.name()
}

// The completed length of all the torrent data, in all its files. This is
// derived from the torrent info, when it is available.
func (t *Torrent) Length() int64 {
	return *t.length
}

// Returns a run-time generated metainfo for the torrent that includes the
// info bytes and announce-list as currently known to the client.
func (t *Torrent) Metainfo() metainfo.MetaInfo {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return t.newMetaInfo()
}

func (t *Torrent) addReader(r *reader) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	if t.readers == nil {
		t.readers = make(map[*reader]struct{})
	}
	t.readers[r] = struct{}{}
	r.posChanged()
}

func (t *Torrent) deleteReader(r *reader) {
	delete(t.readers, r)
	t.readersChanged()
}

func (t *Torrent) DownloadPieces(begin, end int) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	t.downloadPiecesLocked(begin, end)
}

func (t *Torrent) downloadPiecesLocked(begin, end int) {
	for i := begin; i < end; i++ {
		if t.pieces[i].priority.Raise(PiecePriorityNormal) {
			t.updatePiecePriority(i)
		}
	}
}

func (t *Torrent) CancelPieces(begin, end int) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	t.cancelPiecesLocked(begin, end)
}

func (t *Torrent) cancelPiecesLocked(begin, end int) {
	for i := begin; i < end; i++ {
		p := &t.pieces[i]
		if p.priority == PiecePriorityNone {
			continue
		}
		p.priority = PiecePriorityNone
		t.updatePiecePriority(i)
	}
}

func (t *Torrent) initFiles() {
	t.files = t.GetFiles(t.info.Name)
}

// Returns handles to the files in the torrent. This requires the metainfo is
// available first.
func (t *Torrent) GetFiles(root string) (ret *[]*File) { // root - original torrent or new renamed name
	var offset int64
	files := new([]*File)
	for _, fi := range t.info.UpvertedFiles() {
		*files = append(*files, &File{
			t,
			strings.Join(append([]string{root}, fi.Path...), "/"),
			offset,
			fi.Length,
			fi,
			PiecePriorityNone,
		})
		offset += fi.Length
	}
	return files
}

// Returns handles to the files in the torrent. This requires that the Info is
// available first.
func (t *Torrent) Files() []*File {
	return *t.files
}

func (t *Torrent) AddPeers(pp []Peer) {
	cl := t.cl
	cl.mu.Lock()
	defer cl.mu.Unlock()
	t.addPeers(pp)
}

// Marks the entire torrent for download. Requires the info first, see
// GotInfo. Sets piece priorities for historical reasons.
func (t *Torrent) DownloadAll() {
	t.DownloadPieces(0, t.numPieces())
}

func (t *Torrent) String() string {
	s := t.name()
	if s == "" {
		s = t.infoHash.HexString()
	}
	return s
}

func (t *Torrent) AddTrackers(announceList [][]string) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	t.addTrackers(announceList)
}

func (t *Torrent) RemoveTracker(addr string) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()

	nn := make([][]string, 0)

	for _, tier := range t.metainfo.AnnounceList {
		n := make([]string, 0)
		for _, tracker := range tier {
			if tracker != addr {
				n = append(n, tracker)
			} else {
				if trackerScraper, ok := t.trackerAnnouncers[tracker]; ok {
					trackerScraper.stop.Set()
				}
			}
		}
		if len(n) > 0 {
			nn = append(nn, n)
		}
	}

	t.metainfo.AnnounceList = nn
}

type Tracker struct {
	Url          string
	Peers        int
	Err          error
	LastAnnounce int64
	NextAnnounce int64
}

func (t *Torrent) Trackers() []Tracker {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()

	var tt []Tracker

	for _, tier := range t.metainfo.AnnounceList {
		for _, trackerURL := range tier {
			T := Tracker{Url: trackerURL}
			if trackerScraper, ok := t.trackerAnnouncers[trackerURL]; ok {
				T.Peers = trackerScraper.lastAnnounce.NumPeers
				T.Err = trackerScraper.lastAnnounce.Err
				T.LastAnnounce = int64(trackerScraper.lastAnnounce.Completed.Unix())
				T.NextAnnounce = trackerScraper.lastAnnounce.NextAnnounce
			}
			tt = append(tt, T)
		}
	}

	tt = append(tt, Tracker{"DHT", t.stats.PeersDHT, t.stats.ErrDHT, t.stats.LastAnnounceDHT, 0})

	tt = append(tt, Tracker{"PEX", t.stats.PeersPEX, nil, 0, 0})

	return tt
}

type PeerInfo struct {
	Id     [20]byte
	Name   string
	Addr   string
	Source peerSource
	// Peer is known to support encryption.
	SupportsEncryption bool
	PiecesCompleted    int
	// byte info information
	Downloaded int64
	Uploaded   int64
}

func (t *Torrent) Peers() []PeerInfo {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	v := make([]PeerInfo, 0, len(t.conns))
	for value, _ := range t.conns {
		var encrypted bool
		if value.cryptoMethod == mse.CryptoMethodRC4 {
			encrypted = true
		} else if value.headerEncrypted {
			encrypted = true
		}
		v = append(v, PeerInfo{value.PeerID, value.PeerClientName,
			value.remoteAddr().String(), value.Discovery, encrypted,
			value.stats.PiecesCompleted, value.stats.BytesRead, value.stats.BytesWritten})
	}
	return v
}

func (t *Torrent) Check() bool {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return t.checking
}

func (t *Torrent) Stop() {
	t.closed.Set()
}

func (t *Torrent) PiecePended(i int) bool {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return t.pendingPieces.Contains(i)
}

func (t *Torrent) PieceBytesCompleted(i int) int64 {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	p := t.pieces[i]
	return int64(p.length() - p.bytesLeft())
}

func (t *Torrent) PieceLength(i int) int64 {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return int64(t.pieces[i].length().Int())
}

func (t *Torrent) GetPendingPieces() (ret bitmap.Bitmap) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	t.pendingPieces.IterTyped(func(piece int) (more bool) {
		ret.Set(piece, true)
		return true
	})
	return
}

func (t *Torrent) SetChunkSize(c pp.Integer) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	t.chunkSize = c
}

func (t *Torrent) UpdateAllPieceCompletions() {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	t.updateAllPieceCompletions()
}

func (t *Torrent) RemoveCompleted(s int, e int) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	t.completedPieces.RemoveRange(s, e)
	for i := s; i < e; i++ {
		t.pieces[i].dirtyChunks.Clear()
	}
}

func (t *Torrent) UpdatePiecePriorities() {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	t.updatePiecePriorities(0, len(t.pieces))
}

func (t *Torrent) SetStats(downloaded, uploaded int64) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	t.stats.BytesRead = downloaded
	t.stats.BytesWritten = uploaded
}

func (t *Torrent) LoadInfoBytes(buf []byte) error {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return t.setInfoBytes(buf)
}

func (t *Torrent) AnnounceList() (al [][]string) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return t.metainfo.AnnounceList
}

func (t *Torrent) SetMaxConns(max int) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	t.maxEstablishedConns = max
}

func (t *Torrent) Wait() <-chan struct{} {
	return t.closed.LockedChan(&t.cl.mu)
}

func (t *Torrent) Piece(i int) *Piece {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()
	return &t.pieces[i]
}

func (t *Torrent) WriteChunk(offset int64, buf []byte, chunks [][]int64) {
	t.cl.mu.Lock()
	defer t.cl.mu.Unlock()

	bufLen := int64(len(buf))

	ps := offset / t.info.PieceLength // start piece
	pn := ps + 1                      // start + 1 == next piece

	end := offset + bufLen          // end of buffer
	pend := pn * t.info.PieceLength // 'next piece' offset

	if pend > end {
		pend = end
	}

	pieceOffset := offset - ps*t.info.PieceLength // offset in bytes from start of pieceIndex
	pieceLen := pend - offset                     // first piece len. less then len(buf). do not overlap pieces

	for bufLen > 0 {
		chunk := buf[:pieceLen]

		pieceIndex := int(offset / t.info.PieceLength)
		piece := &t.pieces[pieceIndex]

		piece.incrementPendingWrites()

		if len(chunks[pieceIndex]) == 0 {
			chunks[pieceIndex] = make([]int64, piece.numChunks())
		}
		chunkFirst := int(pieceOffset / int64(t.chunkSize))            // first chunk (can be partial size)
		chunkStart := chunkFirst + 1                                   // first full size chunk
		chunkEnd := int((pieceOffset + pieceLen) / int64(t.chunkSize)) // last chunk (can be partial size) index
		chunkRest := pieceLen                                          // current piece len
		{
			spec := piece.chunkIndexSpec(chunkFirst)
			l := int64(chunkFirst)*int64(t.chunkSize) + int64(spec.Length) - pieceOffset
			if l > chunkRest {
				l = chunkRest // first partial chunk is smaller then whole packed
			}
			chunks[pieceIndex][chunkFirst] += l
			chunkRest -= l
			if chunks[pieceIndex][chunkFirst] >= int64(spec.Length) {
				piece.unpendChunkIndex(chunkFirst) // unpend only fully downloaded chunks
			}
		}
		for i := chunkStart; i < chunkEnd; i++ { // start from first full chunk < last partial chunk
			l := int64(piece.chunkIndexSpec(i).Length)
			chunks[pieceIndex][i] += l
			chunkRest -= l
			piece.unpendChunkIndex(i) // unpend only fully downloaded chunks
		}
		if chunkRest > 0 { // chunkRest partial size of chunkEnd
			chunks[pieceIndex][chunkEnd] += chunkRest
			spec := piece.chunkIndexSpec(chunkEnd)
			if chunks[pieceIndex][chunkEnd] >= int64(spec.Length) {
				piece.unpendChunkIndex(chunkEnd) // unpend only fully downloaded chunks
			}
		}

		t.stats.readBytes(pieceLen)
		t.cl.stats.readBytes(pieceLen)

		// Write the chunk out. Note that the upper bound on chunk writing
		// concurrency will be the number of connections.
		var err error
		func() { // auto unlock/lock. panic() mutex lost
			t.cl.mu.Unlock()
			defer t.cl.mu.Lock()
			err = t.writeChunk(pieceIndex, pieceOffset, chunk)
		}()

		piece.decrementPendingWrites()

		if err != nil { // piece can be completed, skip error, continue with rest data
			log.Printf("%s (%x): error writing chunk %s", t, t.infoHash, err)
			t.updatePieceCompletion(pieceIndex)
		}

		// It's important that the piece is potentially queued before we check if
		// the piece is still wanted, because if it is queued, it won't be wanted.
		if t.pieceAllDirty(pieceIndex) {
			t.queuePieceCheck(pieceIndex)
		}

		buf = buf[pieceLen:]
		bufLen = int64(len(buf))
		offset = offset + pieceLen
		pieceOffset = 0
		pieceLen = t.info.PieceLength // next chunk length == full piece length
		if pieceLen > bufLen {
			pieceLen = bufLen // next chunk length == rest of buffer
		}
	}
}
