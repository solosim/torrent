package torrent

type TorrentStats struct {
	// Aggregates stats over all connections past and present. Some values may
	// not have much meaning in the aggregate context.
	ConnStats

	// Ordered by expected descending quantities (if all is well).
	TotalPeers       int
	PendingPeers     int
	ActivePeers      int
	ConnectedSeeders int
	HalfOpenPeers    int

	// How many active peers we have from DHT source (currenly just last value from DHT request)
	PeersDHT int
	// error message
	ErrDHT error
	// last call
	LastAnnounceDHT int64
	// How many active peers we have from PEX source (currenly just last value from pex request)
	PeersPEX int
}
